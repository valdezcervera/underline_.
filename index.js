
var _ = {};

// ARRAYS

// _.first(array, [n])
// Returns an array with the first n elements of an array.
// If n is not provided it returns an array with just the first element.
_.first = function (array, n) {
  // isNaN(void 0) == true, works for n=undefined. return empty array or first element depending on isArray negated.
  if (isNaN(n) || n<=0) return !Array.isArray(array) ? [] : [array[0]];
  // [].slice.call(args), slice array from 0 to n 
  //_________________________¿ What is the diference bewteen [].func.call -- Array.prototype.func.call && func.call ?
  return Array.prototype.slice.call(array, 0, n);
};

// Returns an array with the last n elements of an array.
// If n is not provided it returns an array with just the last element.
_.last = function (array, n) {
  if (isNaN(n) || n<=0) return !Array.isArray(array) ? [] : [array[array.length-1]];
  if (n>=array.length) return array;
  //slice the first "length-n" elements of the array. Alength not working here for arguments object test (¿¿??) 
  return Array.prototype.slice.call(array, array.length-n);
};

// _.uniq(array)
// Produces a duplicate-free version of the array, using === to test equality.
// In particular only the first occurence of each value is kept.
_.uniq = function (array) {
  //pass a callback function to _.filter, which looks at the indexOf value of each element of the collection,
  //if the element is present more than once, its indexOf value will remain the same, and won't match it's index
  //position.
  return _.filter(array, (element, position, collection) => collection.indexOf(element) === position);
};

// OBJECTS

// _.extend(destination, source)
// Copies all the own enumerable properties in the source object over
// to the destination object, and returns it (without using `Object.assign`).
_.extend = function (destination, source) {
  for (var key in source) {
    //Ignore object.prototype
    if (source.hasOwnProperty(key)) {
      destination[key] = source[key];
    }
  }
  return destination;
};

// _.defaults(destination, source)
// Fills in undefined properties in the destination object
// with own enumerable properties present in the source object,
// and returns the destination object.
_.defaults = function (destination, source) {
  for (var key in source) {
    //Ignore object.prototype
    if (source.hasOwnProperty(key)) {
      //Fill in undefined properties
      if (destination[key] == void 0) destination[key] = source[key];
    }
  }
  return destination;
};

// COLLECTIONS

// _.each(collection, iteratee, [context])
// Iterates over a collection of elements (i.e. array or object),
// yielding each in turn to an iteratee function, that is called with three arguments:
// (element, index|key, collection), and bound to the context if one is passed.
// Returns the collection for chaining.
_.each = function (obj, iteratee, context) {
  let i; 
  let length;
  if (Array.isArray(obj)) {
    for (i = 0, length = obj.length; i < length; i++) {
      iteratee.call(context, obj[i], i, obj);
    }
  } else {
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) { 
        iteratee.call(context, obj[key], key, obj);
      }
    }
  }
  return obj;
};

// _.contains(collection, value)
// Returns an array of indexes / keys where value can be found in the collection.
// TIP: here's a demo of how you can re-use already implemented methods in clever ways.
_.contains = function (collection, value) {
  var res = [];
  _.each(collection, function (el, key) {
    el === value && res.push(key);
  });
  return res;
};

// _.map(collection, iteratee, [context])
// Returns a new array of values by mapping each value in collection through iteratee.
// Each invocation of iteratee is called with three arguments:
// (element, index|key, collection), and bound to the context if one is passed.
_.map = function (collection, iteratee, context) {  
  //var keys = !isArrayLike(obj) && _.keys(obj),  //returns array with collection/(obj) keys
  //implement the above case, (isArray not implemented yet):
  // var keys = [];
  // for (var key in collection) if (collection.hasOwnProperty(key)) keys.push(key);
  // var  length = (keys || collection).length; //keys.length||obj.length
  // var  results = Array(length); // creates an array with 'length' empty slots
  // for (var index = 0; index < length; index++) {  
  //   var currentKey = keys ? keys[index] : index;  //store object keys if true, array index if false
  //   results[index] = iteratee.call(context, collection[currentKey], currentKey, collection); //results[index]: each slot is empty, on each 
  // //loop do iteratee.call on each element -obj[currentKey], and returns key and obj for use with other functions.
  // }

  // return results;

  // TIP: here's a demo of how you can re-use already implemented methods in clever ways.
  var res = [];
  _.each(collection, function (el, key) {
    res.push(iteratee.call(context, el, key, collection));
  });
  return res;

};

// _.reduce(collection, iteratee, [accumulator], [context])
// Reduce boils down a collection of values into a single value.
// Accumulator is the initial state of the reduction,
// and each successive step of it should be returned by iteratee.
// Iteratee is passed four arguments: (accumulator, element, index|key, collection),
// and bound to the context if one is passed. If no accumulator is passed
// to the initial invocation of reduce, iteratee is not invoked on the first element,
// and the first element is instead passed as accumulator for the next invocation.
_.reduce = function (collection, iteratee, accumulator, context) {
//   var keys = [];
//   for (var key in collection) if (collection.hasOwnProperty(key)) keys.push(key);
//   var  length = (keys || collection).length; //keys.length||obj.length
//   var index = 0;
//   if (!accumulator) {
//     accumulator = collection[keys ? keys[index] : index];  // store first index on accumulator
//     index ++ ;  // avoid iteratee from adding first index to itself... start iteration from index 1 ahead
//   }
//   for (;index < length; index ++ ) { // index has already been assigned a value, no need to do it here.
//     var currentKey = keys ? keys[index] : index; // make sure if its an array or an object we are iterating
//     accumulator = iteratee.call(context, accumulator, collection[currentKey], currentKey, collection);
//   }
//   return accumulator;
// };
  // Check if an accumulator value has been passed, if not, assign the first element to it
  _.each(collection, function (element, index, collection) {
    if (accumulator==void 0) {
      accumulator = element;
    } else {
    //call the iteratee on the args
      accumulator = iteratee.call(context, accumulator, element, index, collection);
    }
  });
  return accumulator;
};

// _.filter(collection, predicate, [context])
// Looks through each value in the collection, returning an array of all the values
// that pass a truth test (predicate). Predicate is called with three arguments:
// (element, index|key, collection), and bound to the context if one is passed.
_.filter = function (collection, predicate, context) {
  var results = [];
  //predicate = cb(predicate, context) returns --> predicate.call(context, element, index, collection)
  _.each(collection, function (element, index, collection) {
    if (predicate.call(context, element, index, collection)) results.push(element);
  });
  return results;
};

// _.reject(collection, predicate, [context])
// Looks through each value in the collection, returning an array of all the values
// that don't pass a truth test (predicate). Predicate is called with three arguments:
// (element, index|key, collection), and bound to the context if one is passed.
// TIP: can you reuse _.filter()?
_.reject = function (collection, predicate, context) {
  const negated = function (predicate) { // num => num %2 == 0
    return function () { 
      return !predicate.apply(this, arguments); // apply this predicate to argument list
    };// returns num %2 !== 0  
  };
  return _.filter(collection, negated(predicate), context);
};

// _.every(collection, [predicate], [context])
// Returns true if all values in the collection pass the predicate truth test.
// Predicate is called with three arguments:
// (element, index|key, collection), and bound to the context if one is passed.
// Short-circuits and stops traversing the list if a false element is found.
// TIP: without the short-circuiting you could reuse _.reduce(). Can you figure how?
// Because of the short-circuiting though, you need to re-implement a modified _.each().
_.every = function (collection, predicate, context) {
  //=========================== With _.reduce (useless...)========================================
  // return _.reduce(collection, function (accumulator, element, index) {
  //   return predicate.call(context, accumulator, element, index); 
  // });
  //============================== Modified _.each==============================================
  //---if the collection is not an array, get the keys for later. (ignores object proto???)
  let keys = !Array.isArray(collection) && Object.keys(collection);
  //---get the length, if it is an object, we can iterate it by looking at its keys.length
  let length = (keys || collection).length;
  for (let i = 0; i < length; i++) {
  //---do we have the keys?, if so get each key by its position. else return array index  
    var currentKey = keys ? keys[i] : i;
    //---call the predicate negated, if false it will short circuit. (not completley clear how...)   
    if ( !predicate.call(context, collection[currentKey], currentKey, collection) ) return false;
  }
  //---if no short circuit occurs, it's true...
  return true;
};

// _.some(collection, [predicate], [context])
// Returns true if any value in the collection passes the predicate truth test.
// Predicate is called with three arguments:
// (element, index|key, collection), and bound to the context if one is passed.
// Short-circuits and stops traversing the list if a true element is found.
// TIP: what method that you have already implemented can be reused here?
_.some = function (collection, predicate, context) {
  let keys = !Array.isArray(collection) && Object.keys(collection);
  let length = (keys || collection).length;
  for (let i = 0; i < length; i++) {
    var currentKey = keys ? keys[i] : i;
    if ( predicate.call(context, collection[currentKey], currentKey, collection) ) return true;
  }
  return false;

};

// _.invoke(collection, methodName, *arguments)
// Returns an array with the results of calling the method
// indicated by methodName on each value in the collection.
// Any extra arguments passed to invoke will be forwarded on to the method invocation.

//not passing test, but works fine on the browser console =\
_.invoke = function (collection, methodName) {
  if (Array.isArray(methodName) ) {
    if (methodName.length>=2) var argsToMethod = methodName.slice(1); 
    var method = methodName[0];
  } else {method = methodName;}
  if (!Array.isArray(collection)) collection =_.map(collection, _.each);
  var invokedMethod = collection[method];
  return invokedMethod.call(collection, argsToMethod);
}; 

//===========================================================================================
// myInvoke = function (collection, methodName) {
//   if (Array.isArray(methodName) ) {
//   // _.rest could be usefull here...  
//     if (methodName.length>=2) var argsToMethod = methodName.slice(1); 
//     var method = methodName[0];
//   } else {method = methodName;}
//   if (!Array.isArray(collection)) collection =_.map(collection, _.each);
//   // use _.each to apply custom functions to each element of collection...
//   if (method instanceof Function) {
//        return _.each(collection, method)
//     }else{
//       var invokedMethod = collection[method];
//     }
//   return invokedMethod.call(collection, argsToMethod);
// };

// console.log(myInvoke([1,2,3], 'join','-','*'));
//============================================================================================  

// _.pluck(collection, propertyName)
// A convenient version of what is perhaps the most common use-case for map:
// given an array of objects (collection), iterates over each element
// in the collection, and returns an array with all the values
// corresponding to the property indicated by propertyName.
_.pluck = function (collection, propertyName) {
  //taken from underscore's shallowProperty: each time _.map iterates over an object of
  //the array, it checks for the property to be there, if so, return it (obj[key]), else,
  //return 'void 0' which falls to undefined.
  function isShallow (key) {
    return function (obj) {
      return obj == null ? void 0 : obj[key];
    };
  }
  // map the array and pass each element to 'isShallow' function.
  return _.map(collection, isShallow(propertyName));
};

// FUNCTIONS

// _.once(func)
// Creates a version of the function that can only be called one time.
// Repeated calls to the modified function will have no effect,
// returning the value from the original call. Useful for initialization functions,
// instead of having to set a boolean flag and then check it later.
_.once = function (func) {
  //save the computation of the first call so we can return it if it is called again.
  var result;
  return function () { 
    //evaluate if the function has been called.
    if (func) {
      //no need for context or 'this'
      result = func.apply(null, arguments);
      //disable the function
      func = null;
    }
    return result;
  };
};

// _.memoize(func)
// Memoizes a given function by caching the computed result.
// Useful for speeding up slow-running computations.
// You may assume that the memoized function takes only one argument
// and that it is a primitive. Memoize should return a function that when called,
// will check if it has already computed the result for the given argument
// and return that value instead of recomputing it.
_.memoize = function (func) {
  var cache = {};
  return function () {
    var key = JSON.stringify(arguments); //save arguments with object sintax
    if (cache[key]) {   // checks if value has been computed
      return cache[key];// return value from cache object
    }
    else {
      var  val = func.apply(null, arguments); // execute function on arguments, dont use 'this'
      cache[key] = val; // cache computation
      return val; // return computed value (func.apply result)
    }
  };
};

// _.delay(function, wait, *arguments)
// Much like setTimeout(), invokes function after waiting milliseconds.
// If you pass the optional arguments, they will be forwarded
// on to the function when it is invoked.
_.delay = function (func, wait) {
  //extract the parameter from 'arguments' variable
  var param = arguments[2];
  //simply return setTimeout with the function, the delay and extra parameters,
  //as shown in MDN documentation.
  return setTimeout(func, wait, param);
};

// _.throttle(function, wait)
// Returns a new, throttled version of the passed function that,
// when invoked repeatedly, will only call the original function
// at most once per every wait milliseconds, and otherwise will
// just return the last computed result. Useful for rate-limiting
// events that occur faster than you can keep up with.
_.throttle = function (func, wait) {
  var isCalled = 0;
  var result;
  //set the amount of throttle we want. Wait n mSeconds to reset isCalled to 0.
  //(this is where the actual throttle occurs)
  setTimeout(function () {
    return isCalled = 0;
  }, wait);
  //throttle callback:
  //checks the value of isCalled, if its the first call it will compute the result
  //and save it on 'result' variable.
  //in the case that isCalled == 1, no computation occurs and the 'result' variable
  //is returned with the last computation.
  return function () {
    if (isCalled == 0) {
      isCalled++;
      result = func.apply(null, arguments);
    }
    return result;
  }; 

};

// Allow tests to run on the server (leave at the bottom)
if (typeof window === 'undefined') {
  module.exports = _;
}
